-- ETAT ----------------------------------------------------------------------------------------------------------------

create table unicaen_evenement_etat
(
    ID serial not null,
    CODE VARCHAR(255) not null,
    LIBELLE VARCHAR(255) not null,
    DESCRIPTION VARCHAR(2047)
);
create unique index UEVENEMENT_ETAT_ID_UINDEX on unicaen_evenement_etat (ID);
create unique index UEVENEMENT_ETAT_CODE_UINDEX  on unicaen_evenement_etat (CODE);
alter table unicaen_evenement_etat add constraint UEVENEMENT_ETAT_PK primary key (ID);


-- TYPE ----------------------------------------------------------------------------------------------------------------

create table unicaen_evenement_type
(
    id serial not null constraint pk_evenement_type primary key,
    code varchar(255) not null constraint un_evenement_type_code unique deferrable initially deferred,
    libelle varchar(255) not null,
    description varchar(2047),
    parametres varchar(2047),
    recursion varchar(64)
);

-- INSTANCE

create table unicaen_evenement_instance
(
    id serial not null constraint pk_evenement_instance primary key,
    nom varchar(255) not null,
    description varchar(1024) not null,
    type_id integer not null constraint fk_evenement_instance_type references unicaen_evenement_type deferrable,
    etat_id integer not null constraint fk_evenement_instance_etat references unicaen_evenement_etat deferrable,
    parametres text,
    mots_clefs text,
    date_creation timestamp not null,
    date_planification timestamp not null,
    date_traitement timestamp,
    log text,
    parent_id integer constraint fk_evenement_instance_parent references unicaen_evenement_instance deferrable,
    date_fin timestamp
);

create index ix_evenement_instance_type on unicaen_evenement_instance (type_id);
create index ix_evenement_instance_etat on unicaen_evenement_instance (etat_id);
create index ix_evenement_instance_parent on unicaen_evenement_instance (parent_id);

-- JOURNAL

create table unicaen_evenement_journal
(
    id serial not null constraint unicaen_evenement_journal_pk primary key,
    date_execution timestamp not null,
    log text,
    etat_id integer constraint unicaen_evenement_journal_unicaen_evenement_etat_id_fk references unicaen_evenement_etat on delete set null
);

create unique index unicaen_evenement_journal_id_uindex on unicaen_evenement_journal (id);