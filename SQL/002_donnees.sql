-- ETAT

INSERT INTO unicaen_evenement_etat (CODE, LIBELLE, DESCRIPTION) VALUES ('en_attente', 'Événement en attente de traitement', null);
INSERT INTO unicaen_evenement_etat (CODE, LIBELLE, DESCRIPTION) VALUES ('en_cours', 'Événement en cours de traitement', 'L''événement est en cours de traitement et sera mis à jour après celui-ci');
INSERT INTO unicaen_evenement_etat (CODE, LIBELLE, DESCRIPTION) VALUES ('echec', 'Événement dont le traitement a échoué', null);
INSERT INTO unicaen_evenement_etat (CODE, LIBELLE, DESCRIPTION) VALUES ('succes', 'Événement dont le traitement a réussi', null);
INSERT INTO unicaen_evenement_etat (CODE, LIBELLE, DESCRIPTION) VALUES ('annule', 'Événement dont le traitement a été annulé', null);

