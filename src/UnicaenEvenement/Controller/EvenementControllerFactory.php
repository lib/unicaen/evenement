<?php

namespace UnicaenEvenement\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Form\Evenement\EvenementForm;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\EvenementCollection\EvenementCollectionService;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionService;
use Interop\Container\ContainerInterface;
use UnicaenEvenement\Service\Type\TypeService;

class EvenementControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EvenementController
    {
        /**
         * @var EtatService $etatService
         * @var EvenementCollectionService $evenementCollectionService
         * @var EvenementGestionService $evenementGestionService
         * @var EvenementService $evenementService
         * @var TypeService $typeService
         */
        $etatService = $container->get(EtatService::class);
        $evenementCollectionService = $container->get(EvenementCollectionService::class);
        $evenementService = $container->get(EvenementService::class);
        $evenementGestionService = $container->get(EvenementGestionService::class);
        $typeService = $container->get(TypeService::class);

        /**
         * @var EvenementForm $evenementForm
         */
        $evenementForm = $container->get('FormElementManager')->get(EvenementForm::class);

        $controller = new EvenementController();
        $controller->setEtatEvenementService($etatService);
        $controller->setEvenementCollectionService($evenementCollectionService);
        $controller->setEvenementGestionService($evenementGestionService);
        $controller->setEvenementService($evenementService);
        $controller->setTypeService($typeService);

        $controller->setEvenementForm($evenementForm);

        return $controller;
    }
}