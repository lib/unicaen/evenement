<?php

namespace UnicaenEvenement\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\Journal\JournalService;
use UnicaenEvenement\Service\Type\TypeService;
use Interop\Container\ContainerInterface;

class IndexControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IndexController
    {
        /**
         * @var EtatService $etatService
         * @var EvenementService $evenementService
         * @var JournalService $journalService
         * @var TypeService $typeService
         */
        $etatService = $container->get(EtatService::class);
        $evenementService = $container->get(EvenementService::class);
        $journalService = $container->get(JournalService::class);
        $typeService = $container->get(TypeService::class);

        $controller = new IndexController();
        $controller->setEtatEvenementService($etatService);
        $controller->setEvenementService($evenementService);
        $controller->setJournalService($journalService);
        $controller->setTypeService($typeService);
        return $controller;
    }
}