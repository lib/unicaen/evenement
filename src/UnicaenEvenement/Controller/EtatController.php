<?php

namespace UnicaenEvenement\Controller;

use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Form\Etat\EtatForm;
use UnicaenEvenement\Form\Etat\EtatFormAwareTrait;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class EtatController extends AbstractActionController {
    use EtatServiceAwareTrait;
    use EtatFormAwareTrait;

    public function ajouterAction(): ViewModel
    {
        $etat = new Etat();

        $form = $this->getEtatForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-evenement/etat/ajouter', [], [], true));
        $form->bind($etat);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatEvenementService()->create($etat);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-evenement/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'un état d'événement",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $etat = $this->getEtatEvenementService()->getRequestedEtat($this);

        $form = $this->getEtatForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-evenement/etat/modifier', ['etat' => $etat->getId()], [], true));
        $form->bind($etat);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatEvenementService()->update($etat);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-evenement/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'un état d'événement",
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $etat = $this->getEtatEvenementService()->getRequestedEtat($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getEtatEvenementService()->delete($etat);
            exit();
        }

        $vm = new ViewModel();
        if ($etat !== null) {
            $vm->setTemplate('unicaen-evenement/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'etat " . $etat->getCode(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-evenement/etat/supprimer', ["etat" => $etat->getId()], [], true),
            ]);
        }
        return $vm;
    }
}