<?php

namespace UnicaenEvenement\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Form\Etat\EtatForm;
use UnicaenEvenement\Service\Etat\EtatService;
use Interop\Container\ContainerInterface;

class EtatControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtatController
    {
        /**
         * @var EtatService $etatService
         * @var EtatForm $etatForm
         */
        $etatService = $container->get(EtatService::class);
        $etatForm = $container->get('FormElementManager')->get(EtatForm::class);

        $controller = new EtatController();
        $controller->setEtatEvenementService($etatService);
        $controller->setEtatForm($etatForm);
        return $controller;
    }
}