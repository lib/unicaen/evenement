<?php

namespace UnicaenEvenement\Controller;

use UnicaenEvenement\Entity\Db\Type;
use UnicaenEvenement\Form\Type\TypeFormAwareTrait;
use UnicaenEvenement\Service\Type\TypeServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class TypeController extends AbstractActionController {
    use TypeServiceAwareTrait;
    use TypeFormAwareTrait;

    public function ajouterAction(): ViewModel
    {
        $type = new Type();

        $form = $this->getTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-evenement/type/ajouter', [], [], true));
        $form->bind($type);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getTypeService()->create($type);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-evenement/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'un type d'événement",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $type = $this->getTypeService()->getRequestedType($this);

        $form = $this->getTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-evenement/type/modifier', ['type' => $type->getId()], [], true));
        $form->bind($type);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getTypeService()->update($type);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-evenement/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'un type d'événement",
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction() : ViewModel
    {
        $type = $this->getTypeService()->getRequestedType($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getTypeService()->delete($type);
            exit();
        }

        $vm = new ViewModel();
        if ($type !== null) {
            $vm->setTemplate('unicaen-evenement/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du type " . $type->getCode(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-evenement/type/supprimer', ["type" => $type->getId()], [], true),
            ]);
        }
        return $vm;
    }
}