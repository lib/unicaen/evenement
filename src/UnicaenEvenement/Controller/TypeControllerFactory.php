<?php

namespace UnicaenEvenement\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Form\Type\TypeForm;
use UnicaenEvenement\Service\Type\TypeService;
use Interop\Container\ContainerInterface;

class TypeControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TypeController
    {
        /**
         * @var TypeService $etatService
         * @var TypeForm $etatForm
         */
        $etatService = $container->get(TypeService::class);
        $etatForm = $container->get('FormElementManager')->get(TypeForm::class);

        $controller = new TypeController();
        $controller->setTypeService($etatService);
        $controller->setTypeForm($etatForm);
        return $controller;
    }
}