<?php

namespace UnicaenEvenement\Controller;

use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;
use UnicaenEvenement\Service\Journal\JournalServiceAwareTrait;
use UnicaenEvenement\Service\Type\TypeServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    use EtatServiceAwareTrait;
    use EvenementServiceAwareTrait;
    use JournalServiceAwareTrait;
    use TypeServiceAwareTrait;


    public function indexAction() : ViewModel
    {
        $types = $this->getTypeService()->getEntityRepository()->findAll();
        $etats = $this->getEtatEvenementService()->getEntityRepository()->findAll();

        $filtre = $this->params()->fromQuery();
        if (!isset($filtre['date']) OR $filtre['date'] === '') $filtre['date'] = '1M';

        $evenements = $this->getEvenementService()->getEvenementsWithFiltre($filtre);
        $journaux = $this->getJournalService()->getJounaux();

        return new ViewModel([
            'events' => ($evenements)??[],
            'countEvents' => count(($evenements)??[]),
            'types' => ($types)??[],
            'etats' => ($etats)??[],
            'params' => $filtre,
            'journaux' => $journaux,
        ]);
    }
}