<?php

namespace UnicaenEvenement\Controller;

use DateTime;
use Exception;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Form\Evenement\EvenementFormAwareTrait;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;
use UnicaenEvenement\Service\EvenementCollection\EvenementCollectionServiceAwareTrait;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEvenement\Service\Type\TypeServiceAwareTrait;

class EvenementController extends AbstractActionController {
    use EtatServiceAwareTrait;
    use TypeServiceAwareTrait;
    use EvenementServiceAwareTrait;
    use EvenementGestionServiceAwareTrait;
    use EvenementCollectionServiceAwareTrait;
    use EvenementFormAwareTrait;

    public function afficherAction() : ViewModel
    {
        $evenement = $this->evenementService->find($this->params()->fromRoute('evenement'));
        $title = "Détail de l'événement";

        return new ViewModel([
            'title' => $title,
            'evenement' => $evenement,
        ]);
    }

    public function traiterAction() : ViewModel
    {
        $evenements = $this->getEvenementService()->getRequestedEvenements($this, 'ids');
        $data = [];

        try {
            foreach ($evenements as $evenement) {
                if ($evenement) {
                    // on ne traite que les événements en attente
                    if ($evenement->getEtat()->getCode() == Etat::EN_ATTENTE AND $evenement->getDatePlanification() <= (new DateTime())) {
                        $evenement = $this->evenementGestionService->traiterEvent($evenement);
                        $etat = $evenement->getEtat();
                        $data[$evenement->getId()] = [
                            "evenement" => $evenement->getId(),
                            "etat" => $evenement->getEtat()->getLibelle(),
                            "log" => $evenement->getLog(),

                        ];
                        $data[$evenement->getId()] = [$etat->getCode(), $etat->getLibelle()];
                    }
                    else{
                        $data[$evenement->getId()] = [
                            "evenement" => $evenement->getId(),
                            "etat" => $evenement->getEtat()->getLibelle(),
                            "log" => "Traitement ignoré car l'événement n'est pas en attente",
                        ];
                    }
                }
            }

            $vm = new ViewModel([
                'message' => "Traitement des événements sélectionnés",
                'status' => 'success',
                'result' => $data,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
        catch (Exception $e) {
            $vm =  new ViewModel([
                'message' => "Une erreur est survenue lors du traitement des événements sélectionnés.",
                'status' => 'error',
                'result' => $data,
                'exception' => $e,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
    }

    public function changerEtatAction() : ViewModel
    {
        $evenements = $this->getEvenementService()->getRequestedEvenements($this, 'ids');
        $etatId = $this->params()->fromPost('etat', 1);
        $etat = $this->getEtatEvenementService()->find($etatId);
        $data = [];

        try {
            foreach ($evenements as $evenement) {
                $evenement->setDateTraitement(null);
                $evenement = $this->evenementGestionService->changerEtatEvent($evenement, $etat);
                $etat = $evenement->getEtat();
                $data[$evenement->getId()] = [$etat->getCode(), $etat->getLibelle()];
            }
            $vm =  new ViewModel([
                'message' => "État des événements sélectionnés modifié avec succès.",
                'status' => 'success',
                'result' => $data,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
        catch (Exception $e) {
            $vm = new ViewModel([
                'message' => "Une erreur est survenue lors de la modification de l'état des événements sélectionnés",
                'status' => 'error',
                'result' => $data,
                'exception' => $e,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
    }

    public function supprimerAction() : ViewModel
    {
        $evenements = $this->getEvenementService()->getRequestedEvenements($this, 'ids');
        $data = [];

        $this->evenementService->beginTransaction(); // suspend auto-commit

        try {
            foreach ($evenements as $evenement) {
                $evenement = $this->evenementGestionService->supprimerEvent($evenement);
                $data[$evenement->getId()] = [$evenement->getNom()];
            }

            // commit
            $this->evenementService->commit();

            $vm = new ViewModel([
                'message' => "Événements sélectionnés supprimés avec succès.",
                'status' => 'success',
                'result' => $data,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
        catch (Exception $e) {
            $this->evenementService->rollBack();
            $vm = new ViewModel([
                'message' => "Une erreur est survenue lors de la suppression des événements sélectionnés.",
                'status' => 'error',
                'exception' => $e,
            ]);
            $vm->setTemplate('unicaen-evenement/evenement/reponse.phtml');
            return $vm;
        }
    }

    public function ajouterAction() : ViewModel
    {
        $type = $this->getTypeService()->getRequestedType($this);

        $instance = new Evenement();
        $instance->setType($type);

        $form = $this->getEvenementForm();
        $form->setAttribute('action',$this->url()->fromRoute('unicaen-evenement/evenement/ajouter', ['type' => $type->getId()], [], true));
        $form->bind($instance);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $etat = $this->getEtatEvenementService()->findByCode(Etat::EN_ATTENTE);
                $instance->setEtat($etat);
                $instance->setNom($type->getLibelle());
                $instance->setDescription($type->getDescription());
                $instance->setDateCreation(new DateTime());
                $this->getEvenementService()->create($instance);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'un événement de type ".$type->getCode(),
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-evenement/default/default-form');
        return $vm;
    }
}