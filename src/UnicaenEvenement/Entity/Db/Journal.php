<?php

namespace UnicaenEvenement\Entity\Db;

use DateTime;

class Journal {

    private ?int $id = null;
    private ?DateTime $date = null;
    private ?string $log = null;
    private ?Etat $etat = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getLog(): ?string
    {
        return $this->log;
    }

    public function setLog(string $log): void
    {
        $this->log = $log;
    }

    public function getEtat(): ?Etat
    {
        return $this->etat;
    }

    public function setEtat(Etat $etat): void
    {
        $this->etat = $etat;
    }

}