<?php

namespace UnicaenEvenement\Entity\Db;

class Type {

    const COLLECTION = 'collection';

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?string $parametres = null;
    private ?string $recursion = null;

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getCode() : ?string
    {
        return $this->code;
    }

    public function setCode(string $code) : void
    {
        $this->code = $code;
    }

    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle) : void
    {
        $this->libelle = $libelle;
    }

    public function getDescription() : ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description) : void
    {
        $this->description = $description;
    }

    public function getParametres() : ?string
    {
        return $this->parametres;
    }

    public function setParametres(?string $parametres) : void
    {
        $this->parametres = $parametres;
    }

    public function getRecursion(): ?string
    {
        return $this->recursion;
    }

    public function setRecursion(?string $recursion): void
    {
        $this->recursion = $recursion;
    }

}