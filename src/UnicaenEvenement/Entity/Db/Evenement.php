<?php

namespace UnicaenEvenement\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Evenement {
    const DATE_FORMAT = 'd/m/Y H:i';
    const SEPARATOR = ';';

    private ?int $id = -1;

    private ?string $nom = null;
    private ?string $description = null;
    private ?Type $type = null;
    private ?string $parametres = null;
    private ?string $motsClefs = null;

    private ?Etat $etat;
    private ?string $log = null;

    private ?DateTime $dateCreation = null;
    private ?DateTime $datePlanification = null;
    private ?DateTime $dateTraitement = null;
    private ?DateTime $dateFin = null;

    private ?Evenement $parent = null;
    private Collection $fils;

    public function __construct()
    {
        $this->fils = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    /** DESCRIPTION ***************************************************************************************************/

    public function setNom(?string $nom) : void
    {
        $this->nom = $nom;
    }

    public function getNom() : ?string
    {
        return $this->nom;
    }

    public function getDescription() : ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description) : void
    {
        $this->description = $description;
    }

    public function getType() : ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type) : void
    {
        $this->type = $type;
    }

    public function getParametres() : ?string
    {
        return $this->parametres;
    }

    public function setParametres(?string $parametres) : void
    {
        $this->parametres = $parametres;
    }

    public function getMotsClefs(): ?string
    {
        return $this->motsClefs;
    }

    public function setMotsClefs(array $motsClefs): void
    {
        $this->motsClefs = implode(Evenement::SEPARATOR, $motsClefs);
    }

    public function hasMotClef(string $motclef): bool
    {
        $array = explode(Evenement::SEPARATOR, $this->motsClefs);
        return in_array($motclef, $array);
    }

    /** DATES *********************************************************************************************************/

    public function getDateCreation() : ?DateTime
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?DateTime $dateCreation) : void
    {
        $this->dateCreation = $dateCreation;
    }

    public function getDatePlanification(): ?DateTime
    {
        return $this->datePlanification;
    }

    public function setDatePlanification(DateTime $datePlanification) : void
    {
        $this->datePlanification = $datePlanification;
    }

    public function getDateTraitement(): ?DateTime
    {
        return $this->dateTraitement;
    }

    public function setDateTraitement(?DateTime $dateTraitement) : void
    {
        $this->dateTraitement = $dateTraitement;
    }

    public function getDateFin(): ?DateTime
    {
        return $this->dateFin;
    }

    public function setDateFin(?DateTime $dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    /** LOG et ETAT  **************************************************************************************************/

    public function getEtat() : ?Etat
    {
        return $this->etat;
    }

    public function setEtat(?Etat $etat) : void
    {
        $this->etat = $etat;
    }

    public function getLog() : ?string
    {
        return $this->log;
    }

    public function setLog(?string $log) : void
    {
        $this->log = $log;
    }

    /** ARBRE  ********************************************************************************************************/

    public function getParent() : ?Evenement
    {
        return $this->parent;
    }

    public function setParent(?Evenement $parent) : void
    {
        $this->parent = $parent;
    }

    public function addFils(Evenement $fils) : void
    {
        if(!$this->fils->contains($fils)) {
            $this->fils->add($fils);
        }
    }

    public function removeFils(Evenement $fils) : void
    {
        if($this->fils->contains($fils)) {
            $this->fils->removeElement($fils);
        }
    }

    public function getFils() : Collection
    {
        return $this->fils;
    }
}