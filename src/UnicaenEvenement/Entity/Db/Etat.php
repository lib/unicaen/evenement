<?php

namespace UnicaenEvenement\Entity\Db;

class Etat {

    const EN_ATTENTE    = 'en_attente';
    const EN_COURS      = 'en_cours';
    const ECHEC         = 'echec';
    const SUCCES        = 'succes';
    const ANNULE        = 'annule';

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }


}