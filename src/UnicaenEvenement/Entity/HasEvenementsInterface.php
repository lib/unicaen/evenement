<?php

namespace UnicaenEvenement\Entity;

use Doctrine\Common\Collections\Collection;
use UnicaenEvenement\Entity\Db\Evenement;

interface HasEvenementsInterface {

    public function initEvenements() : void;
    public function hasEvenement(Evenement $evenement): bool;
    public function addEvenement(Evenement $evenement): void;
    public function removeEvenement(Evenement $evenement): void;
    public function getEvenements(): Collection;

}