<?php

namespace UnicaenEvenement\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenEvenement\Entity\Db\Evenement;

trait HasEvenementsTrait {

    public Collection $evenements;

    public function initEvenements() : void
    {
        $this->evenements = new ArrayCollection();
    }

    public function hasEvenement(Evenement $evenement): bool
    {
        return $this->evenements->contains($evenement);

    }
    public function addEvenement(Evenement $evenement): void
    {
        if (!$this->hasEvenement($evenement)) {
            $this->evenements->add($evenement);
        }
    }

    public function removeEvenement(Evenement $evenement): void
    {
        if ($this->hasEvenement($evenement)) {
            $this->evenements->removeElement($evenement);
        }
    }

    public function getEvenements(): Collection
    {
        return $this->evenements;
    }
}