<?php

namespace UnicaenEvenement\Form\Etat;

use Interop\Container\ContainerInterface;

class EtatHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatHydrator
     */
    public function __invoke(ContainerInterface $container): EtatHydrator
    {
        $hydrator = new EtatHydrator();
        return $hydrator;
    }
}