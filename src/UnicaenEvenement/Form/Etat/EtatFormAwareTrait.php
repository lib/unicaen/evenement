<?php

namespace UnicaenEvenement\Form\Etat;

trait EtatFormAwareTrait {

    private EtatForm $etatForm;

    public function getEtatForm(): EtatForm
    {
        return $this->etatForm;
    }

    public function setEtatForm(EtatForm $etatForm): void
    {
        $this->etatForm = $etatForm;
    }

}