<?php

namespace UnicaenEvenement\Form\Etat;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class EtatFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtatForm
    {
        /** @var EtatHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(EtatHydrator::class);

        $form = new EtatForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}