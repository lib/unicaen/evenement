<?php

namespace UnicaenEvenement\Form\Etat;

use UnicaenEvenement\Entity\Db\Etat;
use Laminas\Hydrator\HydratorInterface;

class EtatHydrator implements HydratorInterface {

    public function extract(object $object) : array
    {
        /** @var Etat $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $code = (isset($data['code']) AND trim($data['code']) !== "") ? $data['code'] : null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== "") ? $data['libelle'] : null;
        $description = (isset($data['description']) AND trim($data['description']) !== "") ? $data['description'] : null;

        /** @var Etat $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setDescription($description);
        return $object;
    }


}