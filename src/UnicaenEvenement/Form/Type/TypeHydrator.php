<?php

namespace UnicaenEvenement\Form\Type;

use UnicaenEvenement\Entity\Db\Type;
use Laminas\Hydrator\HydratorInterface;

class TypeHydrator implements HydratorInterface {

    public function extract(object $object) : array
    {
        /** @var Type $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'parametres' => $object->getParametres(),
            'recursion' => $object->getRecursion(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $code = (isset($data['code']) AND trim($data['code']) !== "") ? $data['code'] : null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== "") ? $data['libelle'] : null;
        $description = (isset($data['description']) AND trim($data['description']) !== "") ? $data['description'] : null;
        $parametres = (isset($data['parametres']) AND trim($data['parametres']) !== "") ? $data['parametres'] : null;
        $recursion = (isset($data['recursion']) AND trim($data['recursion']) !== "") ? $data['recursion'] : null;

        /** @var Type $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setParametres($parametres);
        $object->setRecursion($recursion);
        return $object;
    }


}