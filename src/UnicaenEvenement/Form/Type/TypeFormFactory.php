<?php

namespace UnicaenEvenement\Form\Type;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class TypeFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TypeForm
    {
        /** @var TypeHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(TypeHydrator::class);

        $form = new TypeForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}