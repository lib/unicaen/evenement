<?php

namespace UnicaenEvenement\Form\Type;

trait TypeFormAwareTrait {

    private TypeForm $etatForm;

    public function getTypeForm(): TypeForm
    {
        return $this->etatForm;
    }

    public function setTypeForm(TypeForm $etatForm): void
    {
        $this->etatForm = $etatForm;
    }

}