<?php

namespace UnicaenEvenement\Form\Type;

use Interop\Container\ContainerInterface;

class TypeHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return TypeHydrator
     */
    public function __invoke(ContainerInterface $container): TypeHydrator
    {
        $hydrator = new TypeHydrator();
        return $hydrator;
    }
}