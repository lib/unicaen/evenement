<?php

namespace UnicaenEvenement\Form\Type;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class TypeForm extends Form {

    public function init(): void
    {
        //code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code du type * :",
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé du type * :",
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description du type :",
            ],
            'attributes' => [
                'id' => 'description',
            ],
        ]);
        //parametres
        $this->add([
            'type' => Textarea::class,
            'name' => 'parametres',
            'options' => [
                'label' => "Paramètres associés :",
            ],
            'attributes' => [
                'id' => 'description',
            ],
        ]);
        //recursion
        $this->add([
            'type' => Text::class,
            'name' => 'recursion',
            'options' => [
                'label' => "Délai de récusion <span class='icon icon-information text-info' title='Utiliser le format DateInterval ou laisser vide'></span> : ",
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'id' => 'recursion',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);

        //inputfilter
        //todo code unique ...
        $this->setInputFilter((new Factory())->createInputFilter([
            'code'          => [ 'required' => true,  ],
            'libelle'       => [ 'required' => true,  ],
            'description'   => [ 'required' => false,  ],
            'parametres'    => [ 'required' => false,  ],
            'recursion'    => [ 'required' => false,  ],
        ]));
    }
}