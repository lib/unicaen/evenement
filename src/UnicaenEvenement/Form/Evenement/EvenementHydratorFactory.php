<?php

namespace UnicaenEvenement\Form\Evenement;

use Psr\Container\ContainerInterface;

class EvenementHydratorFactory {

    public function __invoke(ContainerInterface $container) : EvenementHydrator
    {
        $hydrator = new EvenementHydrator();
        return $hydrator;
    }
}