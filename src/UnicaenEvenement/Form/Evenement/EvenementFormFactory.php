<?php

namespace UnicaenEvenement\Form\Evenement;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class EvenementFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EvenementForm
    {
        $hydrator = $container->get('HydratorManager')->get(EvenementHydrator::class);

        $form = new EvenementForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}