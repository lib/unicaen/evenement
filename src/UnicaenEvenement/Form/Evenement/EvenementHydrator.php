<?php

namespace UnicaenEvenement\Form\Evenement;

use DateTime;
use Laminas\Hydrator\HydratorInterface;
use UnicaenEvenement\Entity\Db\Evenement;

class EvenementHydrator implements HydratorInterface {

    public function extract(object $object): array
    {
        /** @var Evenement $object */
        $data = [
            'planification' => ($object AND $object->getDatePlanification())?$object->getDatePlanification()->format(Evenement::DATE_FORMAT):null,
            'parametres' => ($object)?$object->getParametres():null,
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $date = (isset($data['planification']))?DateTime::createFromFormat(Evenement::DATE_FORMAT, $data['planification']):null;
        $parametres = (isset($data['parametres']) AND trim($data['parametres']) !== null)?trim($data['parametres']):null;

        /** @var Evenement $object */
        $object->setDatePlanification($date);
        $object->setParametres($parametres);

        return $object;
    }

}