<?php

namespace UnicaenEvenement\Form\Evenement;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\DateTime;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenEvenement\Entity\Db\Evenement;

class EvenementForm extends Form {

    public function init(): void
    {
        //date de planif
        $this->add([
            'type' => DateTime::class,
            'name' => 'planification',
            'options' => [
                'label' => 'Date de planification * :',
                'format' => Evenement::DATE_FORMAT,
            ],
            'attributes' => [
                'type' => 'planification',
            ],
        ]);
        //parametres (todo json => array)
        $this->add([
            'type' => Text::class,
            'name' => 'parametres',
            'options' => [
                'label' => 'Paramètre :',
            ],
            'attributes' => [
                'type' => 'parametres',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);
        //input filtre
        $this->setInputFilter((new Factory())->createInputFilter([
            'planification'      => [ 'required' => true,  ],
            'parametre'          => [ 'required' => false,  ],
        ]));
    }
}