<?php

namespace UnicaenEvenement\Form\Evenement;


trait EvenementFormAwareTrait {

    private EvenementForm $evenementForm;

    public function getEvenementForm(): EvenementForm
    {
        return $this->evenementForm;
    }

    public function setEvenementForm(EvenementForm $evenementForm): void
    {
        $this->evenementForm = $evenementForm;
    }


}