<?php

namespace UnicaenEvenement\Service\EvenementGestion;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use RuntimeException;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Evenement\EvenementService;

class EvenementGestionServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EvenementGestionService
    {
        /** @var array $serviceMap */
        $mapping = $container->get('Config')['unicaen-evenement']['service'];

        if ($mapping === null)
            throw new RuntimeException('Toto tu as oublié la config!');

        $serviceMap = [];
        foreach ($mapping as $key => $serviceName) {
            $serviceMap[$key] = $container->get($serviceName);
        }

        /**
         * @var EtatService $etatService
         * @var EvenementService $evenementService
         */
        $etatService = $container->get(EtatService::class);
        $evenementService = $container->get(EvenementService::class);

        $service = new EvenementGestionService();
        $service->setServiceMap($serviceMap);
        $service->setEtatEvenementService($etatService);
        $service->setEvenementService($evenementService);
        return $service;
    }
}