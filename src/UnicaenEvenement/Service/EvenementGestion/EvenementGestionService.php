<?php

namespace UnicaenEvenement\Service\EvenementGestion;

use DateInterval;
use DateTime;
use Exception;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementServiceInterface;
use UnicaenApp\Exception\RuntimeException;

class EvenementGestionService
{
    use EvenementServiceAwareTrait;
    use EtatServiceAwareTrait;

    private array $serviceMap = [];


    /**
     * @param array $serviceMap
     * @return EvenementGestionService
     */
    public function setServiceMap(array $serviceMap) : EvenementGestionService
    {
        $this->serviceMap = $serviceMap;
        return $this;
    }

    /**
     * @param Evenement $evenement
     * @return Evenement
     */
    public function ajouterEvent(Evenement $evenement) : Evenement
    {
        /** @var EvenementServiceInterface $service */
        $service = $this->serviceMap[$evenement->getType()->getCode()];
        if ($service === null) {
            throw new RuntimeException("Le type d'événement [" . $evenement->getType()->getCode() . "] n'a pas de service d'associé.");
        }

        $retour = $service->ajouter($evenement);

        return $retour;
    }

    /**
     * @param Evenement $evenement
     * @return Evenement
     */
    public function traiterEvent(Evenement $evenement) : Evenement
    {
        /** @var EvenementServiceInterface $service */
        $service = $this->serviceMap[$evenement->getType()->getCode()];
        if ($service === null) {
            throw new RuntimeException("Le type d'événement [" . $evenement->getType()->getCode() . "] n'a pas de service associé.");
        }

        // on ne traite que les événements en attente
        if ($evenement->getEtat()->getCode() == Etat::EN_ATTENTE AND $evenement->getDatePlanification() <= (new DateTime())) {

            $encours = $this->getEtatEvenementService()->findByCode(Etat::EN_COURS);
            $echec = $this->getEtatEvenementService()->findByCode(Etat::ECHEC);
            $evenement->setEtat($encours);
            $this->evenementService->update($evenement);

            try {
                $retour = $service->traiter($evenement);

                if ($retour !== null) {
                    $etat = $this->getEtatEvenementService()->findByCode($retour);
                    $evenement->setEtat($etat);
                    $evenement->setDateTraitement(new DateTime());
                    $this->evenementService->update($evenement);
                }
            } catch (Exception $e) {
                $evenement->setEtat($echec);
                $evenement->setLog($e->getMessage());
                $this->evenementService->update($evenement);
            }

            // gestion des récursions
            if ($evenement->getType()->getRecursion() !== null) {
                try {
                    $newdate = DateTime::createFromFormat('d/m/Y à H:i:s', $evenement->getDatePlanification()->format('d/m/Y à H:i:s'));
                    $newdate->add(new DateInterval($evenement->getType()->getRecursion()));

                } catch(Exception $e) {
                    throw new RuntimeException("Un problème est survenu lors du calcul de la prochaine planification", 0, $e);
                }
                if ($evenement->getDateFin() === null OR $evenement->getDateFin() > $newdate) {
                    $json_parametres = json_decode($evenement->getParametres());
                    $parametres = [];
                    if ($json_parametres) {
                        foreach ($json_parametres as $param => $value) {
                            $parametres[$param] = $value;
                        }
                    }
                    $newevenement = $this->getEvenementService()->createEvent($evenement->getNom(), $evenement->getDescription(),
                        $this->getEtatEvenementService()->findByCode(Etat::EN_ATTENTE), $evenement->getType(),
                        ($parametres) ?: null, $newdate);
                    if ($evenement->getDateFin()) $newevenement->setDateFin($evenement->getDateFin());
                    $this->getEvenementService()->create($newevenement);
                }
            }
        }

        return $evenement;
    }

    /**
     * @return Evenement[]
     */
    public function traiterEvents() : array
    {
        $evenements = $this->getEvenementService()->getEvenements();
        foreach ($evenements as $evenement) $this->traiterEvent($evenement);
        return $evenements;
    }

    /**
     * @param Evenement $evenement
     * @param Etat $etat
     * @return Evenement
     */
    public function changerEtatEvent(Evenement $evenement, Etat $etat) : Evenement
    {

        /** @var EvenementServiceInterface $service */
        $service = $this->serviceMap[$evenement->getType()->getCode()];
        if ($service === null) {
            throw new RuntimeException("Le type d'événement [" . $evenement->getType()->getCode() . "] n'a pas de service associé.");
        }

        $retour = $service->changerEtat($evenement, $etat);

        return $retour;
    }

    /**
     * @param Evenement $evenement
     * @return Evenement
     */
    public function supprimerEvent(Evenement $evenement) : Evenement
    {
        /** @var EvenementServiceInterface $service */
        $service = $this->serviceMap[$evenement->getType()->getCode()];
        if ($service === null) {
            throw new RuntimeException("Le type d'événement [" . $evenement->getType()->getCode() . "] n'a pas de service associé.");
        }

        $retour = $service->supprimer($evenement);

        return $retour;
    }
}