<?php

namespace UnicaenEvenement\Service\EvenementGestion;

trait EvenementGestionServiceAwareTrait {

    protected EvenementGestionService $evenementGestionService;

    public function getEvenementGestionService() : EvenementGestionService
    {
        return $this->evenementGestionService;
    }

    public function setEvenementGestionService(EvenementGestionService $evenementGestionService) : EvenementGestionService
    {
        $this->evenementGestionService = $evenementGestionService;

        return $this->evenementGestionService;
    }
}