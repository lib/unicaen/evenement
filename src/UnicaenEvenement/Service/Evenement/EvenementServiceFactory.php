<?php

namespace UnicaenEvenement\Service\Evenement;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Type\TypeService;

class EvenementServiceFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EvenementService
    {
        /**
         * @var EntityManager $entityManager
         * @Var EtatService $etatService
         * @Var TypeService $typeService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $etatService = $container->get(EtatService::class);
        $typeService = $container->get(TypeService::class);

        $service = new EvenementService();
        $service->setObjectManager($entityManager);
        $service->setEtatEvenementService($etatService);
        $service->setTypeService($typeService);
        return $service;
    }
}