<?php

namespace UnicaenEvenement\Service\Evenement;

trait EvenementServiceAwareTrait {

    protected EvenementService $evenementService;

    public function getEvenementService() : EvenementService
    {
        return $this->evenementService;
    }

    public function setEvenementService(EvenementService $evenementService) : void
    {
        $this->evenementService = $evenementService;
    }

}