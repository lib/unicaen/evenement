<?php

namespace UnicaenEvenement\Service\Evenement;

use DateInterval;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Entity\Db\Type;
use UnicaenEvenement\Service\CommonService;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Type\TypeServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;

class EvenementService extends CommonService implements EvenementServiceInterface
{
    use EtatServiceAwareTrait;
    use TypeServiceAwareTrait;

    private Type $type;

    /**
     * @param Type $type
     * @return EvenementService
     */
    public function setType(Type $type): EvenementService
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    public function __construct()
    {
    }

    public function getEntityClass() : string
    {
        return Evenement::class;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
       $qb  = $this->getObjectManager()->getRepository($this->getEntityClass())->createQueryBuilder('evenement')
           ->join('evenement.etat', 'etat')->addSelect('etat')
           ->join('evenement.type', 'type')->addSelect('type')
           ->leftJoin('evenement.parent', 'parent')->addSelect('parent')
           ->leftJoin('evenement.fils', 'fil')->addSelect('fil')
       ;
       return $qb;
    }

    /**
     * @param string $champ
     * @param string $order
     * @return Evenement[]
     */
    public function getEvenements(string $champ = 'datePlanification', string $order = 'DESC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('evenement.' . $champ, $order);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $etatCode
     * @param bool $planOk
     * @return array
     */
    public function getEvenementsByEtat(string $etatCode, bool $planOk = true) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etat.code = :code')
            ->setParameter('code', $etatCode)
        ;

        if ($planOk) {
            $qb = $qb->andWhere('evenement.datePlanification <= :date')
                ->setParameter('date', new DateTime())
            ;
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Evenement[]
     */
    public function getRequestedEvenements(AbstractActionController $controller, string $param = 'evenements') : array
    {
        $result = [];
        $ids = $controller->params()->fromRoute($param);
        if(isset($ids)){
            $ids = explode(',' , $ids);
            $qb = $this->createQueryBuilder()
                ->andWhere('evenement.id in (:ids)')
                ->setParameter('ids', $ids)
            ;
            $result = $qb->getQuery()->getResult();
        }
        return $result;
    }

    /**
     * @param array $filtres
     * @return Evenement[]
     */
    public function getEvenementsWithFiltre(array $filtres = []) : array
    {
        $qb = $this->createQueryBuilder();

        if (isset($filtres['etat']) AND $filtres['etat'] !== '') {
            $qb = $qb->andWhere('etat.code = :etat')->setParameter('etat', $filtres['etat']);
        }
        if (isset($filtres['date']) AND $filtres['date'] !== '') {
            try {
                $date = (new DateTime())->sub(new DateInterval('P' . $filtres['date']));
            } catch (Exception $e) {
                throw new RuntimeException("Problème de calcul de la date buttoir avec [".$filtres['date']."]",0,$e);
            }
            $qb = $qb->andWhere('evenement.datePlanification >= :date')->setParameter('date',$date);
        }
        if (isset($filtres['type']) AND $filtres['type'] !== '') {
            $qb = $qb->andWhere('type.id = :type')->setParameter('type', $filtres['type']);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return Evenement|null
     */
    public function getEvenement(?int $id) : ?Evenement
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('evenement.id = :id')
            ->setParameter('id', $id)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Evenement partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Evenement|null
     */
    public function getRequestedEvenement(AbstractActionController $controller, string $param = 'evenement') : ?Evenement
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getEvenement($id);
        return $result;
    }

    //todo jp voir la porter de (display), de (search) et de (process)

    /**
     * Crée un événement
     *
     * @param string $nom
     * @param string $description
     * @param Etat|null $etat
     * @param Type $type
     * @param array|null $parametres
     * @param DateTime|null $dateTraitement
     * @return Evenement
     */
    public function createEvent(string $nom, string $description, ?Etat $etat, Type $type, ?array $parametres, DateTime $dateTraitement = null) : Evenement
    {
        $dateCreation = new DateTime();
        $json = json_encode($parametres);

        if ($etat === null) {
            $etat = $this->getEtatEvenementService()->findByCode(Etat::EN_ATTENTE);
        }

        $evenement = new Evenement();
        $evenement->setNom($nom);
        $evenement->setDescription($description);
        $evenement->setEtat($etat);
        $evenement->setType($type);
        $evenement->setParametres($json);
        $evenement->setDateCreation($dateCreation);
        $evenement->setDatePlanification($dateTraitement ?: $dateCreation);

        return $evenement;
    }

    public function changerEtat(Evenement $evenement, Etat $etat) : Evenement
    {
        $evenement->setEtat($etat);
        $this->getObjectManager()->flush($evenement);
        return $evenement;
    }

    public function ajouter(Evenement $evenement) : Evenement
    {
        $this->create($evenement);
        return $evenement;
    }

    public function supprimer(Evenement $evenement) : Evenement
    {
        $this->delete($evenement);
        return $evenement;
    }

    public function traiter(Evenement $evenement) : string
    {
        return $evenement->getEtat()->getCode();
    }
}
