<?php

namespace UnicaenEvenement\Service\Evenement;

use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;

/**
 * Interface EvenementServiceInterface
 * @package Evenement\Service\Evenement
 */

interface EvenementServiceInterface
{
    /**
     * @param Evenement $evenement
     * @return Evenement
     */
    public function ajouter(Evenement $evenement) : Evenement;

    /**
     * @param Evenement $evenement
     * @param Etat $etat
     * @return Evenement
     */
    public function changerEtat(Evenement $evenement, Etat $etat) : Evenement;

    /**
     * @param Evenement $evenement
     * @return string
     */
    public function traiter(Evenement $evenement) : string;

    /**
     * @param Evenement $evenement
     * @return Evenement
     */
    public function supprimer(Evenement $evenement) : Evenement;
}