<?php

namespace UnicaenEvenement\Service\EvenementCollection;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\Type\TypeService;
use Interop\Container\ContainerInterface;

class EvenementCollectionServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EvenementCollectionService
    {
        /**
         * @var EtatService $etatService
         * @var EvenementService $evenementService
         * @var TypeService $typeService
         */
        $etatService = $container->get(EtatService::class);
        $evenementService = $container->get(EvenementService::class);
        $typeService = $container->get(TypeService::class);

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new EvenementCollectionService();
        $service->setEtatEvenementService($etatService);
        $service->setEvenementService($evenementService);
        $service->setTypeService($typeService);
        $service->setObjectManager($entityManager);

        return $service;
    }

}