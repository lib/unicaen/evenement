<?php

namespace UnicaenEvenement\Service\EvenementCollection;

trait EvenementCollectionServiceAwareTrait {

    protected EvenementCollectionService $evenementCollectionService;


    public function getEvenementCollectionService() : EvenementCollectionService
    {
        return $this->evenementCollectionService;
    }

    public function setEvenementCollectionService(EvenementCollectionService $evenementCollectionService) : void
    {
        $this->evenementCollectionService = $evenementCollectionService;
    }


}