<?php

namespace UnicaenEvenement\Service\EvenementCollection;

use DateTime;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Entity\Db\Type;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;
use UnicaenEvenement\Service\Type\TypeServiceAwareTrait;

class EvenementCollectionService extends EvenementService
{
    use EtatServiceAwareTrait;
    use EvenementServiceAwareTrait;
    use TypeServiceAwareTrait;


    /**
     * Créer un événement
     *
     * @param string $description
     * @param Evenement[] $evenements
     * @param DateTime $dateTraitement
     * @return Evenement
     */
    /**
     * @param string $eventName
     * @param string $description
     * @param array $evenements
     * @param DateTime|null $dateTraitement
     * @return Evenement
     */
    public function creer(string $eventName, string $description, array $evenements, DateTime $dateTraitement = null) : Evenement
    {
        $etat = $this->etatEvenementService->findByCode(Etat::EN_ATTENTE);
        $type = $this->typeService->findByCode(Type::COLLECTION);

        $collectionEvent = $this->evenementService->createEvent($eventName, $description, $etat, $type, [], $dateTraitement ?: new DateTime());

        $parametres = [];
        foreach ($evenements as $event) {
            if($event instanceof Evenement) {
                $parametres['evenements'][] = $event->getId();
                $collectionEvent->addFils($event);
            }
        }

        $collectionEvent->setParametres(json_encode($parametres));

        return $collectionEvent;
    }

    /**
     * Ajouter un événement
     *
     * @param Evenement $evenement
     * @return Evenement
     * @return mixed
     */
    public function ajouter(Evenement $evenement) : Evenement
    {
        $event = $this->evenementService->create($evenement);

        foreach($event->getFils() as $e) {
            $e->setParent($event);
            $this->evenementService->update($e);
        }

        return $event;
    }

    /**
     * Traiter un événement
     *
     * @param Evenement $evenement
     * @return string
     */
    public function traiter(Evenement $evenement) : string
    {
        $contingences = [
            Etat::EN_ATTENTE => 0,
            Etat::EN_COURS => 0,
            Etat::ECHEC => 0,
            Etat::SUCCES => 0,
        ];

        foreach($evenement->getFils() as $e) {
            $contingences[$e->getEtat()->getCode()]++;
        }

        $etatCode = null;
        if ($contingences[Etat::ECHEC] === 0 AND $contingences[Etat::EN_ATTENTE] === 0 AND $contingences[Etat::EN_COURS] === 0) {
            $etatCode = Etat::SUCCES;
        }
        if ($etatCode === null AND $contingences[Etat::ECHEC] > 0) {
            $etatCode = Etat::ECHEC;
        }
        if ($etatCode === null AND $contingences[Etat::EN_COURS] > 0) {
            $etatCode = Etat::EN_COURS;
        }
        if ($etatCode === null) {
            $etatCode = Etat::EN_ATTENTE;
        }

        return $etatCode;
    }

    /**
     * Supprimer un événement
     *
     * @param Evenement $evenement
     * @return Evenement
     */
    public function supprimer(Evenement $evenement) : Evenement
    {
        foreach($evenement->getFils() as $e) {
            $this->evenementService->delete($e);
        }

        $this->evenementService->delete($evenement);

        return $evenement;
    }
}