<?php

namespace UnicaenEvenement\Service\Type;

trait TypeServiceAwareTrait {

    protected TypeService $typeService;

    public function getTypeService(): TypeService
    {
        return $this->typeService;
    }

    public function setTypeService(TypeService $typeService): void
    {
        $this->typeService = $typeService;
    }

}