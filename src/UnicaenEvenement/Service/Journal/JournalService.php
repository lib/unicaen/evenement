<?php

namespace UnicaenEvenement\Service\Journal;

use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Journal;

class JournalService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * @param Journal $journal
     * @return Journal
     */
    public function create(Journal $journal) : Journal
    {
        $this->getObjectManager()->persist($journal);
        $this->getObjectManager()->flush($journal);
        return $journal;
    }

    /**
     * @param Journal $journal
     * @return Journal
     */
    public function update(Journal $journal) : Journal
    {
        $this->getObjectManager()->flush($journal);
        return $journal;
    }

    /**
     * @param Journal $journal
     * @return Journal
     */
    public function delete(Journal $journal) : Journal
    {
        $this->getObjectManager()->remove($journal);
        $this->getObjectManager()->flush($journal);
        return $journal;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Journal::class)->createQueryBuilder('journal');
        return $qb;
    }

    /**
     * @return array
     */
    public function getJounaux() : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('journal.date', 'DESC');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @return Journal|null
     */
    public function getDernierJournal() : ?Journal
    {
        $result = $this->getJounaux();
        if (empty($result)) return null;
        return $result[0];
    }

    /**
     * @return bool
     */
    public function isRunning() : bool
    {
        $last = $this->getDernierJournal();
        return ($last !== null AND $last->getEtat()->getCode() === Etat::EN_COURS);
    }

}