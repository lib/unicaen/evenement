<?php

namespace UnicaenEvenement\Service\Journal;

trait JournalServiceAwareTrait {

    private JournalService $journalService;

    public function getJournalService(): JournalService
    {
        return $this->journalService;
    }

    public function setJournalService(JournalService $journalService): void
    {
        $this->journalService = $journalService;
    }


}