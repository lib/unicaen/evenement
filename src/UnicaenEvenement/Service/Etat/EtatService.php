<?php

namespace UnicaenEvenement\Service\Etat;

use Doctrine\ORM\NonUniqueResultException;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Service\CommonService;
use Laminas\Mvc\Controller\AbstractActionController;

class EtatService extends CommonService
{

    public function getEntityClass() : string
    {
        return Etat::class;
    }

    public function getRequestedEtat(AbstractActionController $controller, string $param = 'etat'): ?Etat
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->find($id);
        return $result;
    }

    public function findByCode(string $code, bool $caseSensitive = false) : ?Etat
    {
        $qb = $this->getObjectManager()->getRepository(Etat::class)->createQueryBuilder('etatevenement')
            ->andWhere('etatevenement.code = :code')
            ->setParameter('code', $code)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Etat partagent le même code [".$code."]",0,$e);
        }
        return $result;


    }
}