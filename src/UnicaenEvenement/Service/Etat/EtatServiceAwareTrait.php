<?php

namespace UnicaenEvenement\Service\Etat;

trait EtatServiceAwareTrait {

    protected EtatService $etatEvenementService;

    public function getEtatEvenementService(): EtatService
    {
        return $this->etatEvenementService;
    }

    public function setEtatEvenementService(EtatService $etatService): void
    {
        $this->etatEvenementService = $etatService;
    }

}