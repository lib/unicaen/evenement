<?php

namespace UnicaenEvenement\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;

abstract class CommonService
{
    use ProvidesObjectManager;
    use EntityManagerAwareTrait;

    /** REPOSITORY ****************************************************************************************************/

    /**
     * EntityRepository
     *
     * @var EntityRepository
     */
    private $repository;

    /**
     * Retourne la classe de l'entité courante
     *
     * @return string
     */
    abstract public function getEntityClass() : string;

    /**
     * Retourne le repository de l'entité courante
     *
     * @return EntityRepository
     */
    public function getEntityRepository() : EntityRepository
    {
        if (!$this->repository) {
            $this->repository = $this->getObjectManager()->getRepository($this->getEntityClass());
        }

        return $this->repository;
    }

    /**
     * Retourne une nouvelle instance de l'entité courante
     *
     * @return mixed
     */
    public function getEntityInstance($name = null)
    {
        $class = ($name) ?: $this->getEntityClass();
        return new $class;
    }

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * Ajoute une entité
     *
     * @param mixed $entity
     * @param string|null $serviceEntityClass
     * @return mixed
     */
    public function create($entity, ?string $serviceEntityClass = null)
    {
        $entityClass = get_class($entity);
        $serviceEntityClass = $serviceEntityClass ?: $this->getEntityClass();
        if ($serviceEntityClass != $entityClass && !is_subclass_of($entity, $serviceEntityClass)) {
            throw new RuntimeException("L'entité transmise doit être de la classe $serviceEntityClass.");
        }

        $this->getObjectManager()->persist($entity);
        $this->getObjectManager()->flush($entity);

        return $entity;
    }

    /**
     * Met à jour une entité
     *
     * @param mixed $entity
     * @param string|null $serviceEntityClass classe de l'entité
     * @return mixed
     */
    public function update($entity, ?string $serviceEntityClass = null)
    {
        $entityClass = get_class($entity);
        $serviceEntityClass = $serviceEntityClass ?: $this->getEntityClass();
        if ($serviceEntityClass != $entityClass && !is_subclass_of($entity, $serviceEntityClass)) {
            throw new RuntimeException("L'entité transmise doit être de la classe $serviceEntityClass.");
        }

        $this->getObjectManager()->flush($entity);
        return $entity;
    }

    /**
     * Supprime une entité
     *
     * @param mixed $entity
     * @param string|null $serviceEntityClass classe de l'entité
     * @return mixed
     */
    public function delete($entity, ?string $serviceEntityClass = null)
    {
        $entityClass = get_class($entity);
        $serviceEntityClass = $serviceEntityClass ?: $this->getEntityClass();
        if ($serviceEntityClass != $entityClass && !is_subclass_of($entity, $serviceEntityClass)) {
            throw new RuntimeException("L'entité transmise doit être de la classe $serviceEntityClass.");
        }

        $this->getObjectManager()->remove($entity);
        $this->getObjectManager()->flush($entity);
        return $entity;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * Initialise une requête
     *
     * @param string $alias Alias d'entité
     * @return QueryBuilder
     */
    public function initQueryBuilder(string $alias) : QueryBuilder
    {
        return $this->getEntityRepository()->createQueryBuilder($alias);
    }

    /**
     * Proxy method.
     *
     * @see EntityManager::beginTransaction()
     */
    public function beginTransaction(): void
    {
        $this->getObjectManager()->beginTransaction();
    }

    /**
     * Proxy method.
     *
     * @see EntityManager::commit()
     */
    public function commit(): void
    {
        $this->getObjectManager()->commit();
    }

    /**
     * Proxy method.
     *
     * @see EntityManager::rollback()
     */
    public function rollback(): void
    {
        $this->getObjectManager()->rollback();
    }

    /**
     * Cherche une entité par son Id
     *
     * @param int $id identifiant de l'entité
     * @return null|mixed
     */
    public function find(int $id)
    {
        return (null != $id)
            ? $this->getEntityRepository()->find($id)
            : null;
    }

    /**
     * Cherche une entité selon un attribut et sa valeur.
     * Possibilité de rendre la recherche insensible à la casse.
     *
     * @param string $value valeur de l'attribut
     * @param string $attribute valeur de l'attribut
     * @param bool $caseSensitive sensibilité à la casse
     * @return mixed|object|null
     */
    public function findByAttribute(string $value, string $attribute, bool $caseSensitive = false)
    {
        if($caseSensitive) {
            $result = $this->getEntityRepository()->findOneBy([$attribute => $value]);
        }
        else {
            $qb = $this->getEntityRepository()->createQueryBuilder($alias = 'octo');
            $qb->andWhere($qb->expr()->eq($qb->expr()->upper("$alias.$attribute"), $qb->expr()->upper(":$attribute")))
                ->setParameter($attribute, $value);

            try {
                $result = $qb->getQuery()->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                throw new RuntimeException("Plusieurs attribut [".$attribute."] pour la valeur [".$value."]",0,$e);
            }
        }

        return $result;
    }

    /**
     * Cherche une entité par son code
     *
     * @param string $code code de l'entité
     * @param bool $caseSensitive sensibilité à la casse
     * @return mixed|object|null
     */
    public function findByCode(string $code, bool $caseSensitive = false)
    {
        $object = $this->findByAttribute($code, 'code', $caseSensitive);
        return $object;
    }

    /**
     * Retourne une liste d'entités sous forme d'un tableau associatif dont les clés
     * sont les id des entités et les valeurs correspondent au champ choisi
     *
     * @param array $entities liste des entités
     * @param string $key attribut utilisé comme clé
     * @param string $value attribut utilisé pour les valeurs
     * @param string $entityClass classe de l'entité
     * @return array
     */
    public function getListForSelect($entities, $key = 'id', $value = 'libelle', $entityClass = null) : array
    {
        $entityClass = $entityClass ?: $this->getEntityClass();

        foreach ($entities as $entity) {
            if ($entity instanceof $entityClass
                && method_exists($entity, $kgetter = 'get' . ucfirst($key))
                && method_exists($entity, $vgetter = 'get' . ucfirst($value))) {
                $result[$entity->$kgetter()] = $entity->$vgetter();
            }
        }

        return (array) $result;
    }

    /**
     * Retourne une liste d'entités sous forme d'un tableau associatif dont
     * les clés sont les id des entités et les valeurs les entités elles-mêmes
     *
     * @param array $entities liste des entités
     * @param string $key attribut utilisé comme clé
     * @param string $entityClass classe de l'entité
     * @return array
     */
    public function getList($entities, $key = 'id', $entityClass = null) : array
    {
        $entityClass = $entityClass ?: $this->getEntityClass();

        foreach ($entities as $entity) {
            if ($entity instanceof $entityClass
                && method_exists($entity, $kgetter = 'get' . ucfirst($key))) {
                $result[$entity->$kgetter()] = $entity;
            }
        }

        return (array)$result;
    }
}