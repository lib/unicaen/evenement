<?php

namespace UnicaenEvenement\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEvenement\Entity\Db\Evenement;

class EvenementsViewHelper extends AbstractHelper
{
    /**
     * @param Evenement[] $evenements
     * @param array $options
     * @return string|Partial
     */
    public function __invoke(array $evenements, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('evenements', ['evenements' => $evenements, 'options' => $options]);
    }
}