<?php

namespace UnicaenEvenement\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenEvenement\Entity\Db\Etat;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

class EtatViewHelper extends AbstractHelper
{
    protected ?Etat $etat = null;

    public function __invoke(?Etat $etat = null): EtatViewHelper
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * @return string|Partial
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @return string|Partial
     */
    public function render(): string|Partial
    {
        if($this->etat == null) {
            return '';
        }

        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etat', ['etat' => $this->etat, 'icon' => $this->renderIcon()]);
    }

    /**
     * @return string
     */
    public function renderIcon(): string
    {
        $class = match ($this->etat->getCode()) {
            Etat::EN_ATTENTE => 'fas fa-pause-circle',
            Etat::EN_COURS => 'fas fa-play-circle',
            Etat::SUCCES => 'fas fa-check-circle',
            Etat::ECHEC => 'fas fa-times-circle',
            Etat::ANNULE => 'fas fa-ban',
            default => 'far fa-question-circle',
        };

        return sprintf('<i class="%s"></i>', $class);
    }
}