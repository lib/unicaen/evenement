<?php

namespace UnicaenEvenement\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenEvenement\Entity\Db\Evenement;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

class EvenementViewHelper extends AbstractHelper
{
    protected ?Evenement $evenement = null;


    public function __invoke(?Evenement $evenement = null): EvenementViewHelper
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * @return string|Partial
     */
    public function __toString()
    {
        return $this->render();
    }

    public function render():  string|Partial
    {
        if($this->evenement == null) {
            return '';
        }

        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('evenement', ['evenement' => $this->evenement]);
    }

    public function renderListe(array $events, array $types, array $etats): string
    {
//        $events = Json::encode($events);
        return $this->view->render('unicaen-evenement/evenement/template/liste', ['events' => $events, 'types' => $types, 'etats' => $etats]);
    }

}