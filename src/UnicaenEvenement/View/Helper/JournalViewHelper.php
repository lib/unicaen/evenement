<?php

namespace UnicaenEvenement\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenEvenement\Entity\Db\Journal;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

class JournalViewHelper extends AbstractHelper
{
    private ?Journal $journal = null;

    public function getJournal(): ?Journal
    {
        return $this->journal;
    }

    public function setJournal(?Journal $journal): void
    {
        $this->journal = $journal;
    }
    
    protected ?string $journalTitle = null;

    public function getJournalTitle() : ?string
    {
        return $this->journalTitle;
    }

    public function setJournalTitle(?string $journalTitle = null): void
    {
        $this->journalTitle = $journalTitle;
    }

    public function __invoke(?Journal $journal = null, ?string $journalTitle=null): JournalViewHelper
    {
        $this->journal = $journal;
        $this->journalTitle = $journalTitle;

        return $this;
    }

    /**
     * @return string|Partial
     */
    public function __toString()
    {
        return $this->render();
    }


    public function render(): string|Partial
    {
        if($this->journal == null) {
            return '';
        }
        
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('journal', ['journal' => $this->journal, 'journalTitle'=>$this->journalTitle]);
    }

    public function renderLog(): null|string|Partial
    {
        if($this->journal == null) {
            return '';
        }
        $log = $this->journal->getLog()?explode("<br/>", $this->journal->getLog()):null;
        $nbLine = $log?count($log):0;
        if($nbLine==0){
            return "--";
        }
        $collapseAtLine=3;
        if ($nbLine <= $collapseAtLine){ return $this->journal->getLog();}
        $collapseClass = uniqid("log-journal-");
        $html = sprintf("<div data-bs-toggle='collapse' 
            data-bs-target='.%s'
            aria-expanded='false' 
            aria-controls='%s'
            style='cursor:pointer'
            >",
            $collapseClass, $collapseClass);
        for($i=0; $i<$collapseAtLine; $i++){
            $html.=$log[$i]."<br>";
        }
        $html .= sprintf("<div class='collapse %s'>", $collapseClass);
        for($i=$collapseAtLine; $i<$nbLine; $i++){
            $html.=$log[$i]."<br>";
        }
        $html .= "</div>";
        $html .= sprintf("<div class='collapse %s show text-muted'>...</div>", $collapseClass);
        $html .= "</div>";
        return $html;
    }
}