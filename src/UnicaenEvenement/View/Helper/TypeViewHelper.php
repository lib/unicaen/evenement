<?php

namespace UnicaenEvenement\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenEvenement\Entity\Db\Type;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

class TypeViewHelper extends AbstractHelper
{
    protected ?Type $type = null;

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): void
    {
        $this->type = $type;
    }


    public function __invoke(?Type $type = null): TypeViewHelper
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|Partial
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @return string|Partial
     */
    public function render(): string|Partial
    {
        if($this->type == null) {
            return '';
        }

        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('type', ['type' => $this->type, 'icon' => $this->renderIcon()]);
    }

    /**
     * @return string
     */
    public function renderIcon(): string
    {
        $class = match ($this->type->getCode()) {
            Type::COLLECTION => "fas fa-bars",
            default => "far fa-question-circle",
        };

        return sprintf('<i class="%s"></i>', $class);
    }
}