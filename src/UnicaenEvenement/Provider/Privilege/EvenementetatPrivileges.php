<?php

namespace UnicaenEvenement\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EvenementetatPrivileges extends Privileges
{
    const ETAT_CONSULTATION = 'evenementetat-etat_voir';
    const ETAT_AJOUT = 'evenementetat-etat_ajouter';
    const ETAT_EDITION = 'evenementetat-etat_modifier';
    const ETAT_SUPPRESSION = 'evenementetat-etat_supprimer';
}