<?php

namespace UnicaenEvenement\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EvenementinstancePrivileges extends Privileges
{
    const INSTANCE_CONSULTATION = 'evenementinstance-instance_consultation';
    const INSTANCE_AJOUT = 'evenementinstance-instance_ajout';
    const INSTANCE_EDITION = 'evenementinstance-instance_edition';
    const INSTANCE_SUPPRESSION = 'evenementinstance-instance_suppression';
    const INSTANCE_TRAITEMENT = 'evenementinstance-instance_traitement';
}
