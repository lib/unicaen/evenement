<?php

namespace UnicaenEvenement\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EvenementtypePrivileges extends Privileges
{
    const TYPE_CONSULTATION = 'evenementtype-type_consultation';
    const TYPE_AJOUT = 'evenementtype-type_ajout';
    const TYPE_EDITION = 'evenementtype-type_edition';
    const TYPE_SUPPRESSION = 'evenementtype-type_suppression';
}
