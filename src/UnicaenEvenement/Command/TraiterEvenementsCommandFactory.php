<?php

namespace UnicaenEvenement\Command;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionService;
use UnicaenEvenement\Service\Journal\JournalService;

class TraiterEvenementsCommandFactory extends Command
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TraiterEvenementsCommand
    {
        $command = new TraiterEvenementsCommand();

        $config = $container->get('Configuration');
        if(isset($config['unicaen-evenement']['max_time_execution'])){
            $max_time_execution = $config['unicaen-evenement']['max_time_execution'];
            $command->setMaxExecutionTime(intval($max_time_execution));
        }

        /**
         * @var EtatService $etatService
         * @var EvenementService $evenementService
         * @var EvenementGestionService $evenementGestionService
         * @var JournalService $journalService
         */
        $etatService = $container->get(EtatService::class);
        $evenementService = $container->get(EvenementService::class);
        $evenementGestionService = $container->get(EvenementGestionService::class);
        $journalService = $container->get(JournalService::class);
        $command->setEtatEvenementService($etatService);
        $command->setEvenementService($evenementService);
        $command->setEvenementGestionService($evenementGestionService);
        $command->setJournalService($journalService);
        return $command;
    }
}
