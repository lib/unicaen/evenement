<?php

namespace UnicaenEvenement\Command;

use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenEvenement\Entity\Db\Journal;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Journal\JournalServiceAwareTrait;

class JournalPurgeCommand  extends Command
{
    protected static $defaultName = 'evenement:purger-journal';

    protected ?DateInterval $conservationTime = null;
    public function setConservationTime(DateInterval $conservationTime): static
    {
        $this->conservationTime = $conservationTime;
        return $this;
    }
    public function getDateSuppression() : DateTime
    {
        $date = new DateTime();
        $conservationTime = ($this->conservationTime) ?? new DateInterval('P1Y');
        $date->sub($conservationTime);
        return $date;
    }

    protected ?string $codeEtat = null;

    public function getCodeEtat(): ?string
    {
        return $this->codeEtat;
    }

    public function setCodeEtat(?string $codeCategorie): static
    {
        $this->codeEtat = $codeCategorie;
        return $this;
    }


    use EtatServiceAwareTrait;
    use JournalServiceAwareTrait;

    protected function configure() : static
    {
        $this->setDescription("Suppression des logs du journal des événements");
        $this->addOption('time', '-t', InputOption::VALUE_OPTIONAL, "Intervale de temps que l'on souhaite conserver les logs -- format : P1Y");
        $this->addOption('etat', '-e', InputOption::VALUE_OPTIONAL, "Code de l'état des entrées de journal que l'on souhaite purger");
        return $this;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        try{
            $time = $input->getOption('time');
            if(isset($time) && $time != ""){
                $this->conservationTime = new DateInterval($time);
            }
            $codeEtat = $input->getOption('etat');
            if(isset($codeEtat) && $codeEtat != ""){
                $this->codeEtat = $codeEtat;
            }
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            exit(-1);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $io->title("Suppression des logs du journal");
            $date = $this->getDateSuppression();
            $codeEtat = $this->getCodeEtat();


            $qb = $this->getJournalService()->createQueryBuilder();
            $qb->andWhere("journal.date < :date");
            $qb->setParameter("date", $date);
            if(isset($codeEtat) && $codeEtat != ""){
                $qb->join('journal.etat', 'etat')->addSelect('etat');
                $qb->andWhere($qb->expr()->eq('etat.code',':codeEtat'));
                $qb->setParameter("codeEtat", $codeEtat);
            };

            $logs = $qb->getQuery()->getResult();

            if(!empty($logs)) {
                $io->progressStart(sizeof($logs));
                /** @var Journal $log */
                foreach ($logs as $log) {
                    $this->getJournalService()->delete($log);
                    $io->progressAdvance();
                }
                $io->progressFinish();
            }
            else{
                $io->text("Aucun log à supprimer");
            };
            $io->success("Suppression terminée");
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            return self::FAILURE;
        }
        return self::SUCCESS;
    }

}