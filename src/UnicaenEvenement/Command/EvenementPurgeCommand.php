<?php

namespace UnicaenEvenement\Command;

use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;

class EvenementPurgeCommand extends Command
{
    protected static $defaultName = 'evenement:purger-evenement';

    protected ?DateInterval $conservationTime = null;
    public function setConservationTime(DateInterval $conservationTime): static
    {
        $this->conservationTime = $conservationTime;
        return $this;
    }
    public function getDateSuppression() : DateTime
    {
        $date = new DateTime();
        $conservationTime = ($this->conservationTime) ?? new DateInterval('P1Y');
        $date->sub($conservationTime);
        return $date;
    }
    protected ?string $codeType = null;
    protected ?string $codeEtat = null;

    public function getCodeType(): ?string
    {
        return $this->codeType;
    }

    public function getCodeEtat(): ?string
    {
        return $this->codeEtat;
    }

    public function setCodeType(?string $codeType): static
    {
        $this->codeType = $codeType;
        return $this;
    }
    public function setCodeEtat(?string $codeCategorie): static
    {
        $this->codeEtat = $codeCategorie;
        return $this;
    }


    use EvenementServiceAwareTrait;

    protected function configure() : static
    {
        $this->setDescription("Suppression des événements traités avant une date");
        $this->addOption('time', '-t', InputOption::VALUE_OPTIONAL, "Intervale de temps que l'on souhaite conserver les événements -- format : P1Y");
        $this->addOption('etat', '-e', InputOption::VALUE_OPTIONAL, "Code de l'état des événements que l'on souhaite purger");
        $this->addOption('type', '-type', InputOption::VALUE_OPTIONAL, "code du type d'événement que l'on souhaite purger");
        return $this;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        try{
            $time = $input->getOption('time');
            if(isset($time) && $time != ""){
                $this->conservationTime = new DateInterval($time);
            }
            $codeEtat = $input->getOption('etat');
            if(isset($codeEtat) && $codeEtat != ""){
                $this->codeEtat = $codeEtat;
            }
            $codeType = $input->getOption('type');
            if(isset($codeType) && $codeType != ""){
                $this->codeType = $codeType;
            }
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            exit(-1);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $io->title("Suppression des anciens événements");

            $date = $this->getDateSuppression();
            $codeEtat = $this->getCodeEtat();
            $codeType = $this->getCodeType();

            $qb = $this->getEvenementService()->createQueryBuilder();

            $qb->andWhere("evenement.dateTraitement < :date");
            $qb->setParameter("date", $date);
            if(isset($codeEtat) && $codeEtat != ""){
                $qb->andWhere($qb->expr()->eq('etat.code',':codeEtat'));
                $qb->setParameter("codeEtat", $codeEtat);
            };
            if(isset($codeType) && $codeType != ""){
                $qb->andWhere($qb->expr()->eq('type.code',':codeType'));
                $qb->setParameter("codeType", $codeType);
            };

            $evenements = $qb->getQuery()->getResult();
            if(!empty($evenements)) {
                $io->progressStart(sizeof($evenements));
                /** @var Evenement $evenement */
                foreach ($evenements as $evenement) {
                    $this->getEvenementService()->delete($evenement);
                    $io->progressAdvance();
                }
                $io->progressFinish();
            }
            else{
                $io->text("Aucun événement à supprimer");
            }
            $io->success("Suppression terminée");
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            return self::FAILURE;
        }
        return self::SUCCESS;
    }
}