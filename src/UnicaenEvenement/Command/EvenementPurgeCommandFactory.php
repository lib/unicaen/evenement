<?php

namespace UnicaenEvenement\Command;

use DateInterval;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenEvenement\Service\Evenement\EvenementService;

class EvenementPurgeCommandFactory extends Command
{
    /**
     * @param ContainerInterface $container
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EvenementPurgeCommand
    {
        $command = new EvenementPurgeCommand();
        $command->setEvenementService($container->get(EvenementService::class));

        $config = $container->get('Configuration');
        if (isset($config['unicaen-evenement']['conservation-time'])) {
            $conservationTime = $config['unicaen-evenement']['conservation-time'];
            if(!$conservationTime instanceof DateInterval) {
                throw new Exception("Le paramètre de coniguration 'unicaen-evenement > conservation-time' doit être un DateInterval");
            }
            $command->setConservationTime($conservationTime);
        }

        return $command;
    }
}