<?php

namespace UnicaenEvenement\Command;

use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Journal;
use UnicaenEvenement\Service\Etat\EtatServiceAwareTrait;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionServiceAwareTrait;
use UnicaenEvenement\Service\Journal\JournalServiceAwareTrait;

class TraiterEvenementsCommand extends Command
{
    use EtatServiceAwareTrait;
    use EvenementServiceAwareTrait;
    use EvenementGestionServiceAwareTrait;
    use JournalServiceAwareTrait;

    /**
     * @var int $maxExecutionTime
     * @desc Temps d'exécution maximal (sec) du script de traitement des événements
     * !! celui-ci doit être en adéquation avec le cycle d'exécution du script de traitement des événements en attente dans la crontab du serveur
     * Cf cle de config ['unicaen-evenement']['max_time_execution']
     * vraleur par défaut : 300s
     * @file /etc/crontab
     */
    protected int $maxExecutionTime = 300;

    public function getMaxExecutionTime(): int
    {
        return $this->maxExecutionTime;
    }

    public function setMaxExecutionTime(int $maxExecutionTime): void
    {
        $this->maxExecutionTime = $maxExecutionTime;
    }

    protected static $defaultName = 'evenement:traiter-evenements';

    protected function configure(): void
    {
        $this->setDescription("Traitement des événements en attente");
        //$this->addArgument('event', InputArgument::OPTIONAL, "ID de l'événement");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        //$event = $input->getArgument('event');

        $io->title("Traitement des événements en attente");

        $journal = $this->getJournalService()->getDernierJournal();
        if ($this->getJournalService()->isRunning()) {
            $io->error("Un traitement est déjà en cours !");
            return self::FAILURE;
        }

        if (isset($journal) && $journal->getEtat()->getCode() == Etat::EN_COURS) {
            $time = new DateTime();
            $executionTime = $time->getTimestamp() - $journal->getDate()->getTimestamp();
            if ($executionTime >= $this->maxExecutionTime) {
                $log = $journal->getLog();
                $log .= "-----------<br/>";
                $log .= sprintf("Mise en echec le %s à %s car temps d'exécution trop long <br/>",
                    $time->format('d/m/Y'), $time->format('H:i')
                );
                $log .= "-----------<br/>";
                $io->warning("Mise en échec du journal #" . $journal->getId());
                $journal->setLog($log);
                $journal->setEtat($this->getEtatEvenementService()->findByCode(Etat::ECHEC));
                $this->getJournalService()->update($journal);
            }
        }

        $texte = "";

        $journal = new Journal();
        $journal->setDate(new DateTime());
        $journal->setEtat($this->getEtatEvenementService()->findByCode(Etat::EN_COURS));
        $this->getJournalService()->create($journal);


        $io->success("Traitement des événements en attente");
        $io->success("Debut du traitement");
        $io->success("-------------------");

        $start = new DateTime(); $nbOp = 0;
        $texte .= "Début du traitement : " . $start->format('d/m/Y H:i:s') . "<br/>";

        $evenements = $this->getEvenementService()->getEvenementsByEtat(Etat::EN_ATTENTE);
        if (empty($evenements)) {
            $io->info("Aucun événément à traiter");
            $journal->setLog("Aucun événément à traiter");
            $journal->setEtat($this->getEtatEvenementService()->findByCode(Etat::SUCCES));
            $this->getJournalService()->update($journal);

            $io->success("-------------------");
            $io->success("Fin du traitement");
            return self::SUCCESS;
        }

        foreach ($evenements as $evenement) {
            // calcul du temps d'exécution
            $executionTime = (new DateTime())->getTimestamp() - $start->getTimestamp();
            if ($executionTime >= $this->maxExecutionTime) {
                // si le temps d'exécution maximal est dépassé ; on quitte la procédure
                break;
            }

            try {
                if ($evenement !== null) {
                    $this->evenementGestionService->traiterEvent($evenement);
                    $io->text("- traitement de l'événement #" . $evenement->getId());
                    $texte .= "Traitement de l'événement #" . $evenement->getId() . " <br/>";
                    $nbOp++;
                }
            } catch (Exception $e) {
                $io->error(sprintf("- échec du traitement de l'événement #%s", $evenement->getId()));
                $io->warning($e->getMessage());
                $texte .= "<strong> Échec du traitement </strong> <br/>";
            }
        }

        $end = new DateTime();
        $texte .= "Fin du traitement : " . $end->format('d/m/Y H:i:s');

        $io->info("Rapport");
        $io->info("-------");
        $io->info("* Nombre d'opérations : " . $nbOp);
        $io->info("* Temps estimé : " .  ($start->diff(new DateTime())->format('%Hh %Im %Ss')));

        $io->success("-------------------");
        $io->success("Fin du traitement");

        $journal->setLog($texte);
        $journal->setEtat($this->getEtatEvenementService()->findByCode(Etat::SUCCES));
        $this->getJournalService()->update($journal);

        return self::SUCCESS;
    }
}