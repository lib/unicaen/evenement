<?php

namespace UnicaenEvenement\Command;

use DateInterval;
use Exception;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenEvenement\Service\Journal\JournalService;

class JournalPurgeCommandFactory extends Command
{
    /**
     * @param ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): JournalPurgeCommand
    {
        $command = new JournalPurgeCommand();
        $command->setJournalService($container->get(JournalService::class));

        $config = $container->get('Configuration');
        if (isset($config['unicaen-evenement']['conservation-time'])) {
            $conservationTime = $config['unicaen-evenement']['conservation-time'];
            if(!$conservationTime instanceof DateInterval) {
                throw new Exception("Le paramètre de coniguration 'unicaen-evenement > conservation-time' doit être un DateInterval");
            }
            $command->setConservationTime($conservationTime);
        }



        return $command;
    }
}