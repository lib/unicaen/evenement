UnicaenEvenement
====
---

Bonne pratique pour créer un nouveau type d'événement
===

1 - Commencer par déclarer un type d'événement
---

Un nouveau type d'événement suppose d'ajouter une instance de ```UnicaenEvenement\Entity\Db\Type```.
Cet ajout peut être réalisé via l'interface du module.
Un type d'événement a besoin des informations suivantes :
* un code
* un libelle
* une description (facultative)
* une liste de paramètres (facultative)
* une période de recursion (facultative) (**TODO modifier le formulaire pour permettre la saisie de la période de récurence**) 

Dans des fin de meilleure implémentation, il est aussi recommendé de déclarer une constante pour avoir accès rapidement au type d'événement.
Ces déclarations peuvent être faite dans le fichier Provider\Event\EventsProvider.php.

```php 
class EventProvider {
    const MON_EVENT = 'mon_event';
}
```

2 - Créer l'événement
---

L'événement peut être placer dans répertoire associé à celui-ci : ```Application\Event\MonEvent```
Celui-ci doit hérité du service ```EvenementService```.

Exemple de création pour MonEvent
```php
<?php

namespace Application\Event\MonEvent;

use Application\Provider\EvenementProvider;
use DateTime;
use Exception;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Entity\Db\Type;
use UnicaenEvenement\Service\Evenement\EvenementService;

class MonEventEvenement extends EvenementService {

    /**
     * @param DateTime|null $dateTraitement
     * @return Evenement
     */
    public function creer(DateTime $dateTraitement = null) : Evenement
    {
        /** @var Type $type */
        $type = $this->getTypeService()->findByCode(EvenementProvider::MON_EVENT);
        $description = "Mon event";
        $evenement = $this->createEvent($description, $description, null, $type, null, $dateTraitement);
        $this->ajouter($evenement);
        return $evenement;
    }

    /**
     * @param Evenement $evenement
     * @return string
     */
    public function traiter(Evenement $evenement) : string
    {
        try {
            //todo écrire procédure de l'événement
        } catch(Exception $e) {
            $evenement->setLog($e->getMessage());
            $this->update($evenement);
            return Etat::ECHEC;
        }
        $evenement->setLog("Ok!");
        $this->update($evenement);
        return Etat::SUCCES;
    }
}
```

Pensez à écrire la factory associée et le trait en cas d'ajout pas une action d'un controller.

3 - Déclaration du lien entre l'événement et le "Service" associé
---

Dans le fichier de configuration ```unicaen-evenement.global.php```, il faut indiquer que le type d'événement créé va exploiter un ```Event``` à celui-ci.

```php
return [
    'unicaen-evenement' => [
        'service' => [
            EventProvider::MON_EVENT => MonEventEvenement::class,
        ],
    ],
];
```

Ce lien est exploité par le module lorsque passe sur les instances d'événement à traiter.
Pour récupérer la méthode ```traiter()``` qui est associé à l'événement.

---

Commande pour lancer le traitement des événements.
===

```sh
$ docker exec preecog ./vendor/bin/laminas evenement:traiter-evenements
```

Déclaration dans la crontab
```sh 
# - Traitement des évènements en attente
*/1 *   * * *   root cd /var/www/html && ./vendor/bin/laminas evenement:traiter-evenements 1> /dev/null 2>&1
```

---

Temps d'exectution des événements
===
Dans le fichier de configuration ```unicaen-evenement.global.php```, modifier la clé 'max_time_execution' pour stoper automatiquement les événements trop long

```php
return [
    'unicaen-evenement' => [
        'max_time_execution' => 300,
    ],
];
```


Purge des événements et des logs du journal
---
Les commandes consoles `evenement:purge` et `evenement:purge` permettent de supprimer les instances d'événement traiter avant une date.

- La durée par défaut de conservertion est de 1 an, il s'agit d'un paramétre modifiable dans la configuration sous la forme de DateInterval :
```
    'unicaen-evenement' => [
        'conservation-time' => new DateInterval('P2Y'),
    ],
```
3 options possibles pour evenement:purge
- `--time=P2Y` *(ou `-t P2Y`)* pour spécifier la durée de conservations des événements (on ignore alors celle définie en conf).
- `--etat=XXX`,  *(ou `-e XXX`)* pour ne purger que les événements dont l'état est de code `XXX` 
- `--type=YYY`,  *(ou `-t YYY`)* pour ne purger que les événements du type dont le code est `YYY` 


2 options possibles pour journal:purge
- `--time=P2Y` *(ou `-t P2Y`)* pour spécifier la durée de conservations des logs (on ignore alors celle définie en conf).
- `--etat=XXX`,  *(ou `-e XXX`)* pour ne purger que les logs dont l'état est de code `XXX`

*** dans tout les cas, on tient compte de la date de traitements des événements lors de la purge ***