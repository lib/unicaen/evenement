<?php

namespace UnicaenEvenement;

use UnicaenEvenement\Controller\EtatController;
use UnicaenEvenement\Controller\EtatControllerFactory;
use UnicaenEvenement\Form\Etat\EtatForm;
use UnicaenEvenement\Form\Etat\EtatFormFactory;
use UnicaenEvenement\Form\Etat\EtatHydrator;
use UnicaenEvenement\Form\Etat\EtatHydratorFactory;
use UnicaenEvenement\Provider\Privilege\EvenementetatPrivileges;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Etat\EtatServiceFactory;
use UnicaenEvenement\View\Helper\EtatViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtatController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => EvenementetatPrivileges::ETAT_AJOUT,
                ],
                [
                    'controller' => EtatController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => EvenementetatPrivileges::ETAT_EDITION,
                ],
                [
                    'controller' => EtatController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => EvenementetatPrivileges::ETAT_SUPPRESSION,
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-evenement' => [
                'child_routes' => [
                    'etat' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/etat',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'ajouter' => [
                                'type'  => Literal::class,
                                'options' => [
                                    'route'    => '/ajouter',
                                    'defaults' => [
                                        'controller' => EtatController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/modifier/:etat',
                                    'defaults' => [
                                        'controller' => EtatController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer/:etat',
                                    'defaults' => [
                                        'controller' => EtatController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            EtatService::class => EtatServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            EtatController::class => EtatControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            EtatForm::class => EtatFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            EtatHydrator::class => EtatHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'etat'          => EtatViewHelper::class,
        ],
    ],
];