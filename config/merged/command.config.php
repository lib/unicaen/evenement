<?php


use UnicaenEvenement\Command\EvenementPurgeCommand;
use UnicaenEvenement\Command\EvenementPurgeCommandFactory;
use UnicaenEvenement\Command\JournalPurgeCommand;
use UnicaenEvenement\Command\JournalPurgeCommandFactory;
use UnicaenEvenement\Command\TraiterEvenementsCommand;
use UnicaenEvenement\Command\TraiterEvenementsCommandFactory;

return [
    'bjyauthorize' => [
        'guards' => [
        ],
    ],

    'laminas-cli' => [
        'commands' => [
            'evenement:traiter-evenements' => TraiterEvenementsCommand::class,
            'evenement:purger-journal' => JournalPurgeCommand::class,
            'evenement:purger-evenement' => EvenementPurgeCommand::class,
        ],
    ],

    'controllers' => [
        'factories' => [
        ],
    ],
    'service_manager' => [
        'factories' => [
            TraiterEvenementsCommand::class => TraiterEvenementsCommandFactory::class,
            JournalPurgeCommand::class => JournalPurgeCommandFactory::class,
            EvenementPurgeCommand::class => EvenementPurgeCommandFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
    ],
];

