<?php

namespace UnicaenEvenement;

use UnicaenEvenement\Controller\TypeController;
use UnicaenEvenement\Controller\TypeControllerFactory;
use UnicaenEvenement\Form\Type\TypeForm;
use UnicaenEvenement\Form\Type\TypeFormFactory;
use UnicaenEvenement\Form\Type\TypeHydrator;
use UnicaenEvenement\Form\Type\TypeHydratorFactory;
use UnicaenEvenement\Provider\Privilege\EvenementtypePrivileges;
use UnicaenEvenement\Service\Type\TypeService;
use UnicaenEvenement\Service\Type\TypeServiceFactory;
use UnicaenEvenement\View\Helper\TypeViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => TypeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => EvenementtypePrivileges::TYPE_AJOUT,
                ],
                [
                    'controller' => TypeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => EvenementtypePrivileges::TYPE_EDITION,
                ],
                [
                    'controller' => TypeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => EvenementtypePrivileges::TYPE_SUPPRESSION,
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-evenement' => [
                'child_routes' => [
                    'type' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/type',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'ajouter' => [
                                'type'  => Literal::class,
                                'options' => [
                                    'route'    => '/ajouter',
                                    'defaults' => [
                                        'controller' => TypeController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/modifier/:type',
                                    'defaults' => [
                                        'controller' => TypeController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer/:type',
                                    'defaults' => [
                                        'controller' => TypeController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            TypeService::class => TypeServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            TypeController::class => TypeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            TypeForm::class => TypeFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            TypeHydrator::class => TypeHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'type'          => TypeViewHelper::class,
        ],
    ],

];