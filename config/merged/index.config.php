<?php

namespace UnicaenEvenement;

use UnicaenEvenement\Controller\IndexController;
use UnicaenEvenement\Controller\IndexControllerFactory;
use UnicaenEvenement\Provider\Privilege\EvenementetatPrivileges;
use UnicaenEvenement\Provider\Privilege\EvenementinstancePrivileges;
use UnicaenEvenement\Service\Journal\JournalService;
use UnicaenEvenement\Service\Journal\JournalServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenEvenement\Provider\Privilege\EvenementtypePrivileges;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EvenementetatPrivileges::ETAT_CONSULTATION,
                        EvenementinstancePrivileges::INSTANCE_CONSULTATION,
                        EvenementtypePrivileges::TYPE_CONSULTATION,
                    ]
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'unicaen-evenement' => [
                                'label' => 'Événement',
                                'route' => 'unicaen-evenement',
                                'resource' => PrivilegeController::getResourceId(IndexController::class, 'index'),
                                'order'    => 11002,
                                'icon' => 'fas fa-angle-right',
                                'pages' => [
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-evenement' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/unicaen-evenement',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            JournalService::class => JournalServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];