<?php

namespace UnicaenEvenement;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEvenement\Controller\EvenementController;
use UnicaenEvenement\Controller\EvenementControllerFactory;
use UnicaenEvenement\Form\Evenement\EvenementForm;
use UnicaenEvenement\Form\Evenement\EvenementFormFactory;
use UnicaenEvenement\Form\Evenement\EvenementHydrator;
use UnicaenEvenement\Form\Evenement\EvenementHydratorFactory;
use UnicaenEvenement\Provider\Privilege\EvenementinstancePrivileges;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenEvenement\Service\Evenement\EvenementServiceFactory;
use UnicaenEvenement\Service\EvenementCollection\EvenementCollectionService;
use UnicaenEvenement\Service\EvenementCollection\EvenementCollectionServiceFactory;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionService;
use UnicaenEvenement\Service\EvenementGestion\EvenementGestionServiceFactory;
use UnicaenEvenement\View\Helper\EvenementsViewHelper;
use UnicaenEvenement\View\Helper\EvenementViewHelper;
use UnicaenEvenement\View\Helper\JournalViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EvenementController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => EvenementinstancePrivileges::INSTANCE_CONSULTATION,
                ],
                [
                    'controller' => EvenementController::class,
                    'action' => [
                        'changer-etat',
                        'traiter',
                        'ajouter',
                    ],
                    'privileges' => EvenementinstancePrivileges::INSTANCE_EDITION,
                ],
                [
                    'controller' => EvenementController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => EvenementinstancePrivileges::INSTANCE_SUPPRESSION,
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-evenement' => [
                'child_routes' => [
                    'evenement' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/evenement',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter/:type',
                                    'defaults' => [
                                        'controller' => EvenementController::class,
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:evenement',
                                    'defaults' => [
                                        'controller' => EvenementController::class,
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'changer-etat' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/changer-etat[/:ids]',
                                    'defaults' => [
                                        'controller' => EvenementController::class,
                                        'action' => 'changer-etat',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer[/:ids]',
                                    'defaults' => [
                                        'controller' => EvenementController::class,
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'traiter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/traiter[/:ids]',
                                    'defaults' => [
                                        'controller' => EvenementController::class,
                                        'action' => 'traiter',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            EvenementService::class => EvenementServiceFactory::class,
            EvenementGestionService::class => EvenementGestionServiceFactory::class,
            EvenementCollectionService::class => EvenementCollectionServiceFactory::class,
        ],
        'abstract_factories' => [
        ]
    ],

    'controllers' => [
        'factories' => [
            EvenementController::class => EvenementControllerFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
            EvenementForm::class => EvenementFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            EvenementHydrator::class => EvenementHydratorFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'evenement' => EvenementViewHelper::class,
            'evenements' => EvenementsViewHelper::class,
            'journal' => JournalViewHelper::class,
        ],
    ],

];